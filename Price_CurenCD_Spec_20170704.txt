Price Currency Feed

KEY
* = primary key
Heading includes REFERENCE, CHANGE or EVENT to denote table type

----------					

Element Name	DataType	Format	Max Width	Lookup TYPEGROUP	Field Description
TableName	VarChar	Char String	10	TABLENAME	Table Name
*SecId	Integer	32 bit	10		Security linking key
ISIN	Char	Char String	12		ISIN from SCMST for reference purposes
*ExchgCD	Char	Char String	6	EXCHG	EDI maintained Exchange code.  Equivalent to the MIC code but necessary as MIC might not be available in a timely fashion.
*Currency	Char	Char String	3	CUREN	The applicable price currency that the bond is denominated in


Use YYYYMMDD_LOOKUP.txt to lookup any the meaning of coded data with an entry in Lookup TYPEGROUP column
Notes on LOOKUP
Combined lookup table where codes of a particular type are grouped by TypeGroup
Join using the Typegroup of the coded field data and the coded data field content to get the lookup
Only field containing coded date has the Typegroup colomn populated.
This table also gives all the POSSIBLE VALUES.
Because it is combining multiple coded types, the format of the field must accomodate the largest possible code in the whole system hence the slight apparent incompatibility in length between to joining fields.


----------					
Series WFI - LOOKUP - Combined Lookup Table -					

Element Name	DataType	Format	Max Width	Lookup TYPEGROUP	Field Description
Actflag	Char	Char String	1	ACTION	Record Level Action Status
Changed	Date	yyyy/mm/dd	10		Last Changed date of record at EDI
*TypeGroup	Char	Char String	10	TYPEGROUP	Link between coded data and Combined Lookup Table
*Code	Char	Char String	10		Code data value
Lookup	VarChar	Char String	70		Lookup value for Code (previous field)
